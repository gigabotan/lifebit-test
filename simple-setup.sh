#!/bin/env bash
SSH_KEY_NAME="lifebit_ed25519"

old_cwd=`pwd`
script_dir=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
packer_dir="${script_dir}/packer"
terraform_dir="${script_dir}/terraform"

exit1() {
  cd "$old_cwd"
  exit $1
}

case "$1" in
install)
    echo "Install packer and terraform from here"
    echo "Packer: https://learn.hashicorp.com/tutorials/packer/get-started-install-cli"
    echo "Terraform: https://learn.hashicorp.com/tutorials/packer/get-started-install-cli"
;;
deploy)
    # Make archive of our app to deploy later
    echo "Preparing archive for deployment"
    tar -zcvf app.tar.gz --directory=src .

    # PACKER
    cd $packer_dir
    packer init .
    packer build aws-ubuntu.pkr.hcl

    # Prepare ssh key for terraform

    ssh-keygen -t ed25519 -C "lifebit" -f ./${SSH_KEY_NAME} -N ""

    # TERRAFORM
    cd $terraform_dir
    sed -i'' -e 's/backend "http"/backend "local"/' main.tf # change backend to local (this sed command should work on both linux and mac)
    terraform init
    terraform apply
;;
test)
    cd $terraform_dir
    echo "Test"
    echo "Open your AWS cloudwatch metrics dashboard and look the metrics of ELB or EC2"
    echo "https://eu-central-1.console.aws.amazon.com/cloudwatch/home?region=eu-central-1#home:dashboards/ApplicationELB"
    echo " "
    echo "Enter params to high load test"
    read -p "How many threads?: " threads
    read -p "How many connections?: " connections
    read -p "How long to run in seconds?: " seconds  
    wrk -t${threads} -c${connections} -d${seconds}s $(terraform output -raw lb_endpoint)
;;
destroy)
    # DESTROY
    cd $terraform_dir
    terraform destroy
;;
*)
    echo "Simple deploy tool for nodejs app on aws autoscaling group"
    echo "Usage: "
    echo "  $0 install        - to install required tools"
    echo "  $0 deploy         - to deploy app to aws"
    echo "  $0 test           - test cluster"
    echo "  $0 destroy        - to destroy cluster and cleanup"
    echo " "
;;
esac

exit1 0