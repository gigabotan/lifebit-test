#!/bin/sh
sleep 30

# Install node js
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.1/install.sh | bash
export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"
nvm install 17
env
# Install pm2
npm install -g pm2
echo `which pm2`

# Configure pm2 to run hellonode on startup
sudo mkdir -p /opt/app
sudo tar -xzf /tmp/app.tar.gz -C /opt/app
sudo chown -R ubuntu:ubuntu /opt/app
sudo chmod -R 755 /opt/app
sudo rm /tmp/app.tar.gz


cd /opt/app
npm install
pm2 start index.js
eval $(pm2 startup | grep sudo)
pm2 save
pm2 list