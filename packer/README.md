# Image building with Packer

Our packer config is [this](aws-ubuntu.pkr.hcl) file

We copy our archived app to image
Then provision autostart of nodejs application by custom script - [setup.sh](setup.sh)

## How to build image

```bash
# Make archive
$ tar -zcvf app.tar.gz --directory=src .

# go to packer dir
$ cd packer
# packer init prepares plugins to deploy your image to AWS
$ packer init .

# building packer image and deploying it to AWS
$ packer build aws-ubuntu.pkr.hcl

```

For more detailed tutorial visit [Packer official tutorial page](https://learn.hashicorp.com/collections/packer/aws-get-started)
