# Advanced usage
## Designing Process

We can only use AWS Free tier, so we have following resources:
 - ec2 (720h)
 - ebs (30gb)
 - s3
 - cloudwatch ($0.00 per metric-month - first 10 metrics)
 - elb(720h)

We need fault tolerance and high availability, that means:
- 2+ replicas
- self-healing group of instances
- load balancer

## Kops
My first approach was to use [Kops](https://kops.sigs.k8s.io), so I can use kubernetes features for scaling, monitoring, load balancing and I dont need to think about low-level infrastucture management on AWS.

But this approach was doomed to fail.
Kops master node needs more resources than we have on free instance(```t2.micro```). Even if I tweak some params of master node(api-server params, resource reservations and etc.) and adding some memory via swap-file, it did not help much.

## K3s
Then I thought I can use [k3s](https://rancher.com/docs/k3s/latest/en/).

It is very lightweight and also easy to install.
But this approach had some huge cons:
- I need to manage both k8s and aws infrastucture myself
- Double amount of work
  - scaling of nodes and scaling of pods
  - ha and fault-tolerance for node and for pods
  - etc.
- Some performance loss because kubelet and other processes on nodes
- We spend resources for master nodes

## Autoscaling groups and AMI with pre-installed app

So I chose the approach without containers and kubernetes.

![asg](https://docs.aws.amazon.com/autoscaling/ec2/userguide/images/elb-tutorial-architecture-diagram.png)
Image from AWS website

This design covers all our requirements:
- Autoscaling groups and Load balancer guarantees high availability and fault tolerance
- Can be used with AWS Free tier
- Easy management by packer and terraform and we can save our setup in repository

## Implementation

### Packer
We use [packer](https://www.packer.io) to build images

[Detailed image preparation docs](packer/README.md)

### Terraform
We use [terraform](https://www.terraform.io) to manage our infrastructure

[Detailed infrastructure and deployment docs](terraform/README.md)

### How to test auto-scaling and by applying load

In this solution I use simple tool - [wrk](https://github.com/wg/wrk)

I did not have time to write different test cases (slowly increasing load and etc.) and only tried some simple cases.

For example, you can achieve increasing desired capacity of ASG from 3 to 4 by using this arguments
```bash
$ wrk -t4 -c100 -d300s $(terraform output -raw lb_endpoint)
```