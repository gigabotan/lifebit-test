# lifebit-test

## LifeBit example service

## Description
 This is solution for test challenge
 Challenge description is [here](test_description.txt)

## Requirements
Before you can build the AMI and deploy infrastructure, you need to provide your AWS credentials to Packer and Terraform.

Follow [this part of terraform tutorial](https://learn.hashicorp.com/tutorials/terraform/aws-build?in=terraform/aws-get-started#prerequisites) to setup you AWS credentials

After this step you can use Packer and Terraform

## Installation
```bash
$ git clone https://gitlab.com/gigabotan/lifebit-test.git
$ cd lifebit-test
$ ./simple-setup.sh deploy
```

## Usage

### Simple Usage
```
Usage: 
  ./simple-setup.sh install        - to install required tools
  ./simple-setup.sh deploy         - to deploy app to aws
  ./simple-setup.sh test           - test cluster
  ./simple-setup.sh destroy        - to destroy cluster and cleanup
  ```

### Detailed usage and explanation
[Look here for design and how its implemented](Solution.md)

## Roadmap
- More useful outputs from terraform
  - ec2 public ip's
  - cluster size
  - current version
- Make variables for more parameters
  - ssh key
  - cluster params
    - capacity
    - region
    - etc
  - security groups for ssh access only from specified IP's

## License
[MIT License](LICENSE.txt)
