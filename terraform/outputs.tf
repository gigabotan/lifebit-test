output "lb_endpoint" {
  value = "http://${module.alb.lb_dns_name}"
}