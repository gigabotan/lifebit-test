terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.27"
    }
  }

  required_version = ">= 0.14.9"
}

provider "aws" {
  profile = var.profile
  region  = var.region
}

terraform {
  backend "http" {
  }
}


module "vpc" {
  source = "terraform-aws-modules/vpc/aws"

  name = "lifebit-vpc"
  cidr = "10.0.0.0/16"

  azs            = ["${var.region}a", "${var.region}b", "${var.region}c"]
  public_subnets = ["10.0.101.0/24", "10.0.102.0/24", "10.0.103.0/24"]

  single_nat_gateway = true

  tags = var.lifebit_tags
}

module "instance_sg" {
  source = "terraform-aws-modules/security-group/aws"

  name        = "instance-sg"
  description = "instance security group"
  vpc_id      = module.vpc.vpc_id
  rules       = var.sg_custom_rules

  ingress_with_cidr_blocks = [
    {
      rule        = "http-3000-tcp"
      cidr_blocks = module.vpc.vpc_cidr_block
    },
    {
      rule        = "ssh-tcp"
      cidr_blocks = "0.0.0.0/0"
    }
  ]

  egress_cidr_blocks = ["0.0.0.0/0"]
  egress_rules       = ["all-all"]

  tags = var.lifebit_tags
}

module "lb_sg" {
  source = "terraform-aws-modules/security-group/aws"

  name        = "lb-sg"
  description = "Security group for load balancer with HTTP ports open within VPC"
  vpc_id      = module.vpc.vpc_id

  ingress_cidr_blocks = ["0.0.0.0/0"]
  ingress_rules       = ["http-80-tcp"]
  egress_cidr_blocks  = ["0.0.0.0/0"]
  egress_rules        = ["all-all"]

  tags = var.lifebit_tags
}

module "alb" {
  source  = "terraform-aws-modules/alb/aws"
  version = "~> 6.0"

  name = "my-alb"

  load_balancer_type = "application"

  vpc_id          = module.vpc.vpc_id
  subnets         = module.vpc.public_subnets
  security_groups = [module.lb_sg.security_group_id]

  target_groups = [
    {
      name_prefix      = "pre-"
      backend_protocol = "HTTP"
      backend_port     = 3000
      target_type      = "instance"
    }
  ]

  http_tcp_listeners = [
    {
      port               = 80
      protocol           = "HTTP"
      target_group_index = 0
    }
  ]

  tags = var.lifebit_tags
}

data "aws_ami" "lifebit_ami" {
  most_recent = true
  owners      = ["606714112736"]

  filter {
    name   = "name"
    values = ["lifebit-nodejs-app*"]
  }
}

// resource "tls_private_key" "this" {
//   algorithm = "RSA"
// }

data "local_file" "public_key" {
  filename = var.ec2_ssh_public_key_path
}

resource "aws_key_pair" "lifebit_key" {
  key_name   = "lifebit"
  public_key = data.local_file.public_key.content
  tags       = var.lifebit_tags
}

// resource "local_file" "ssh_key" {
//   filename = "${aws_key_pair.lifebit_key.key_name}.pem"
//   content = tls_private_key.pk.private_key_pem
// }

module "asg" {
  source = "terraform-aws-modules/autoscaling/aws"

  # Autoscaling group
  name = "lifebit-asg"
  

  min_size                  = 3
  max_size                  = 9
  // desired_capacity          = 3
  wait_for_capacity_timeout = 0
  health_check_type         = "ELB"
  vpc_zone_identifier       = module.vpc.public_subnets

  instance_refresh = {
    strategy = "Rolling"
    preferences = {
      checkpoint_delay       = 60
      checkpoint_percentages = [50, 100]
      instance_warmup        = 30
      min_healthy_percentage = 50
    }
    triggers = ["tag"]
  }

  # Launch template
  launch_template_name        = "lifebit-asg"
  launch_template_description = "Launch template lifebit"
  update_default_version      = true

  image_id        = data.aws_ami.lifebit_ami.id
  instance_type   = "t2.micro"
  security_groups = [module.instance_sg.security_group_id]
  key_name        = aws_key_pair.lifebit_key.key_name

  target_group_arns = module.alb.target_group_arns

  # Enable detailed monitoring for better autoscaling
  enable_monitoring = true

  # Enable for better monitoring
  enabled_metrics = [
    "GroupMinSize",
    "GroupMaxSize",
    "GroupDesiredCapacity",
    "GroupInServiceInstances",
    "GroupTotalInstances"
  ]

  metrics_granularity = "1Minute"

  tags = var.lifebit_tags


  # Target scaling policy schedule based on average CPU load
  scaling_policies = {
    avg-cpu-policy-greater-than-50 = {
      policy_type               = "TargetTrackingScaling"
      estimated_instance_warmup = 300
      target_tracking_configuration = {
        predefined_metric_specification = {
          predefined_metric_type = "ASGAverageCPUUtilization"
        }
        target_value = 50.0
      }
    },
    predictive-scaling = {
      policy_type = "PredictiveScaling"
      predictive_scaling_configuration = {
        mode                         = "ForecastAndScale"
        scheduling_buffer_time       = 10
        max_capacity_breach_behavior = "IncreaseMaxCapacity"
        max_capacity_buffer          = 10
        metric_specification = {
          target_value = 32
          predefined_scaling_metric_specification = {
            predefined_metric_type = "ASGAverageCPUUtilization"
            resource_label         = "testLabel"
          }
          predefined_load_metric_specification = {
            predefined_metric_type = "ASGTotalCPUUtilization"
            resource_label         = "testLabel"
          }
        }
      }
    }
  }
}

