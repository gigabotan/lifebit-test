# Terraform 

## Design

We use terraform to implement our infrastructure design, setup deployment parameters

Terraform configuration is pretty straight forward and simple.
We have all our configuration in [main.tf](main.tf)

Used resources:
- vpc, 3 subnets in different availability zones
- security group for instances
  - open port for our nodejs app from our vpc
  - open ssh port for everyone (can be editor if we want restrict access)
- security group for load balancer
- load balancer with specified target arn
- specified version of our AMI, which is deployed to AWS by Packer in previous step
- key pair for ssh access to instances
- autoscaling group(ASG) with specified scaling options, launch template and load balancer target arn

Deploying new version of our app is done by building new image by Packer and re-running ```terraform apply```
Terraform will detect new version and update launch template for the autoscaling group.
Then ASG will start incremental rolling-update of instances with no downtime of service.

![](https://storage.googleapis.com/cdn.thenewstack.io/media/2017/11/5bddc931-ramped.gif)


## How to deploy and manage our infrastructure

```bash
# Prepare ssh key for terraform
ssh-keygen -t ed25519 -C "lifebit" -f ./${SSH_KEY_NAME} -N ""

# TERRAFORM
$ cd terraform
$ sed -i'' -e 's/backend "http"/backend "local"/' main.tf # change backend to local (this sed command should work on both linux and mac)

# Init our terraform configuration, download modules, setup backend
$ terraform init

# Show what we are planning to create on AWS
$ terraform plan

# Apply this configuration on AWS
$ terraform apply

# We can get our endpoint to check our app via terraform output
$ terraform output lb_endpoint

# Delete all infrastructure that we created
$ terraform destroy

```

## Need help?

You can check [official tutorial](https://learn.hashicorp.com/collections/terraform/aws-get-started)